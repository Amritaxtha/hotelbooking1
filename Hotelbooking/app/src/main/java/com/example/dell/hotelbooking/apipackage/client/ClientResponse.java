package com.example.dell.hotelbooking.apipackage.client;

import com.example.dell.hotelbooking.apipackage.client.ClientData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClientResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("datas")
    @Expose
    private ClientData datas;
    @SerializedName("status")
    @Expose
    private Integer status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ClientData getDatas() {
        return datas;
    }

    public void setDatas(ClientData datas) {
        this.datas=datas;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}