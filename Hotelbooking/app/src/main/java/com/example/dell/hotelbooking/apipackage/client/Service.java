package com.example.dell.hotelbooking.apipackage.client;



import com.example.dell.hotelbooking.model.login.LoginResponse;
import com.example.dell.hotelbooking.model.login.Userdata;
import com.example.dell.hotelbooking.model.register.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;


/**
 * Created by Dell on 18/07/2017.
 */

public interface Service {
   @POST("user/create")
   Observable<ClientResponse> RegisterData(@Body User user);
   @POST("authenticate")
   Observable<LoginResponse> getLoginResponse(@Body Userdata userdata);
}

