package com.example.dell.hotelbooking;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.dell.hotelbooking.adapter.DashboardListAdapter;
import com.example.dell.hotelbooking.adapter.ImageSliderAdapter;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private static ViewPager mPager;
    private static int currentPage = 0;
    TabLayout tabLayout;
   ImageView imageView;
    ListView list;
    SharedPreferences sharedPreferences;


    String[] web = {
            "Reservation",
            "Hotel Information",
            "Services",
            "Contact Us"
    } ;
    Integer[] imageId = {
            R.drawable.ic_action_this,
            R.drawable.ic_action_this,
            R.drawable.ic_action_this,
            R.drawable.ic_action_this,


    };



    private static final Integer[] XMEN = {R.drawable.hotel1, R.drawable.hotel, R.drawable.hotel3, R.drawable.hotel5, R.drawable.hotel6};
    private ArrayList<Integer> XMENArray = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        DashboardListAdapter adapter = new
                DashboardListAdapter(MainActivity.this, web, imageId);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                int itemPosition = position;

                //Listview clicked item value
                String itemValue = (String)list.getItemAtPosition(position);
                switch(itemPosition) {

                    case 0:
                        Intent appInfo = new Intent(MainActivity.this, ActivityHotelReservation.class);
                        startActivity(appInfo);
                        break;
                    case 1:
                        Intent appInfo1 = new Intent(MainActivity.this, ActivityRegistration.class);
                        startActivity(appInfo1);
                        break;
                    case 2:
                        Intent appInfo2 = new Intent(MainActivity.this,ActivityServices.class);
                        startActivity(appInfo2);

                        break;

                    case 3:
                        Intent appInfo3 = new Intent(MainActivity.this, ActivityContact.class);
                        startActivity(appInfo3);
                        break;
                }


            }
        });

    }

    private void init() {
        for (int i = 0; i < XMEN.length; i++)
            XMENArray.add(XMEN[i]);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new ImageSliderAdapter(MainActivity.this, XMENArray));
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == XMEN.length) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2500, 2500);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}







