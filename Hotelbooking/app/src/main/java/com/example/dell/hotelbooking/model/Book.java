package com.example.dell.hotelbooking.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Dell on 13/07/2017.
 */

public class Book implements Parcelable {
    String id;
    String numberroom;

    public Book(String id, String numberroom) {
        this.id = id;
        this.numberroom = numberroom;
    }

    public Book(String numberroom) {
        this.numberroom = numberroom;

    }


    protected Book(Parcel in) {
        numberroom = in.readString();
        id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(numberroom);
        dest.writeString(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };

    public String getNumberroom() {
        return numberroom;
    }

    public void setNumberroom(String numberroom) {
        this.numberroom = numberroom;
    }





    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
