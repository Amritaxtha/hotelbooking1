
package com.example.dell.hotelbooking;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class ActivityServices extends AppCompatActivity implements AdapterView.OnItemClickListener{
    Integer[] imageIDs = {
            R.drawable.breakfast,
            R.drawable.lunch,
            R.drawable.lundery,
            R.drawable.hotelcleaning,
            R.drawable.dinner,


    };

    String[] titles = {
            "BreakFast",
            "Lunch",
            "Lundery",
            "RoomCleaning",
            "Dinner",


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);
        GridView gridView = (GridView) findViewById(R.id.grid);
        gridView.setAdapter(new ImageAdapter(this));

    }
    public class ImageAdapter extends BaseAdapter
    {
        private Context context;

        public ImageAdapter(Context c)
        {
            context = c;
        }

        //---returns the number of images---
        public int getCount() {
            return imageIDs.length;
        }

        //---returns the ID of an item---
        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        //---returns an ImageView view---
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ImageView icon;
            icon = new ImageView(context);

            LayoutInflater inflater = (LayoutInflater)             context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row=inflater.inflate(R.layout.row, parent, false);
            TextView label=(TextView)row.findViewById(R.id.image_name);
            label.setText(titles[position]);
            icon=(ImageView)row.findViewById(R.id.album_image);

            icon.setImageResource(imageIDs[position]);

            return row;
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
