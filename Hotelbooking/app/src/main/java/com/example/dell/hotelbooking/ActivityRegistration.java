package com.example.dell.hotelbooking;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dell.hotelbooking.apipackage.client.APIClient;
import com.example.dell.hotelbooking.apipackage.client.ClientResponse;
import com.example.dell.hotelbooking.apipackage.client.Service;
import com.example.dell.hotelbooking.model.register.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class ActivityRegistration extends AppCompatActivity {
    EditText rusername, rpassword, remail;
    Button register;
    TextView alreadymember;
    boolean uflag = false, eflag = false, pflag = false, rflag = false;
    Service service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        rusername = (EditText) findViewById(R.id.Rusername);
        remail = (EditText) findViewById(R.id.Remail);
        rpassword = (EditText) findViewById(R.id.Rpassword);

        register = (Button) findViewById(R.id.register);
        alreadymember = (TextView) findViewById(R.id.alreadymember);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateUsername(rusername.getText().toString())) {
                    uflag = true;
                } else {
                    uflag = false;
                    rusername.setError("Invalid username");
                }
                if (validateEmail(remail.getText().toString())) {
                    eflag = true;
                } else {
                    eflag = false;
                    remail.setError("Incorrect pattern");
                }
                if (validatePassword(rpassword.getText().toString())) {
                    pflag = true;
                } else {

                    pflag = false;
                    rpassword.setError("password should contain more than 9 characters");
                }

                if (uflag && eflag && pflag == true) {
                    Toast.makeText(ActivityRegistration.this, "hello", Toast.LENGTH_SHORT).show();

                    User user = new User();
                    user.setEmail(remail.getText().toString());
                    user.setUsername(remail.getText().toString());
                    user.setPassword(rpassword.getText().toString());
                    ProgressDialog progressDialog = new ProgressDialog(ActivityRegistration.this);
                    progressDialog.setMessage("Processing...");
                    service = APIClient.getClient().create(Service.class);
                    service.RegisterData(user).observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(new Subscriber<ClientResponse>() {
                                @Override
                                public void onCompleted() {
                                    Toast.makeText(ActivityRegistration.this, "You data is send", Toast.LENGTH_SHORT).show();

                                }

                                @Override
                                public void onError(Throwable e) {

                                    Toast.makeText(ActivityRegistration.this,e.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onNext(ClientResponse clientResponse) {
                                    Toast.makeText(ActivityRegistration.this, clientResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }

            protected boolean validateUsername(String rusername) {
                if (rusername.isEmpty()) {
                    return false;
                } else {
                    return true;

                }

            }

            protected boolean validateEmail(String remail) {
                String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
                Pattern pattern = Pattern.compile(emailPattern);
                Matcher matcher = pattern.matcher(remail);
                if (matcher.matches()) {
                    return true;

                } else {
                    return false;

                }
            }

            protected boolean validatePassword(String rpassword)
            {
                if (rpassword != null && rpassword.length() > 6) {
                    return true;
                } else {
                    return false;
                }
            }

            protected boolean validateReTypePassword(String rretypepassword)
            {
                if (rretypepassword.equals(rpassword.getText().toString())) {

                    return true;
                } else {
                    return false;

                }
            }


        });
        }
    }