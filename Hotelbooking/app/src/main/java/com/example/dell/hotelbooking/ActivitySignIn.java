package com.example.dell.hotelbooking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dell.hotelbooking.apipackage.client.APIClient;
import com.example.dell.hotelbooking.apipackage.client.ClientResponse;
import com.example.dell.hotelbooking.apipackage.client.Service;
import com.example.dell.hotelbooking.model.login.LoginResponse;
import com.example.dell.hotelbooking.model.login.Userdata;
import com.example.dell.hotelbooking.model.register.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ActivitySignIn extends AppCompatActivity {


    EditText email, password;
    CheckBox checkBox;
    Button Sign_in;
    TextView createnewacc;
    boolean Eflag=false;
    boolean Pflag=false;
    Service service;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        email= (EditText) findViewById(R.id.email);
        password= (EditText) findViewById(R.id.password);
        checkBox= (CheckBox) findViewById(R.id.checkbox);
        Sign_in= (Button) findViewById(R.id.signin);
        Sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateEmail(email.getText().toString()))

                {
                    Eflag = true;
                } else {
                    Eflag = false;
                    email.setError("Invalid username");
                    email.requestFocus();
                }
                if (validatePassword(password.getText().toString())) {
                    Pflag = true;
                } else {
                    Pflag = false;
                    password.setError("Invalid password");
                    password.requestFocus();
                }
                if (Eflag && Pflag==true) {
                    Toast.makeText(ActivitySignIn.this, "hello", Toast.LENGTH_SHORT).show();
                    Userdata userdata = new Userdata();
                    userdata.setEmail(email.getText().toString());

                    userdata.setPassword(password.getText().toString());
                    service = APIClient.getClient().create(Service.class);
                    service.getLoginResponse(userdata)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(new Subscriber<LoginResponse>() {


                                @Override
                                public void onCompleted() {

                                    Intent intent = new Intent(ActivitySignIn.this, ActivitySelectRooms.class);
                                    startActivity(intent);

                                }

                                @Override
                                public void onError(Throwable e) {
                                    Toast.makeText(ActivitySignIn.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                                }

                                @Override
                                public void onNext(LoginResponse loginResponse) {

                                }
                            });


                }

                createnewacc = (TextView) findViewById(R.id.createnewacc);
                createnewacc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ActivitySignIn.this, ActivityRegistration.class);
                        startActivity(intent);
                    }
                });
            }

            protected boolean validateEmail(String remail) {
                String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
                Pattern pattern = Pattern.compile(emailPattern);
                Matcher matcher = pattern.matcher(remail);
                if (matcher.matches()) {
                    return true;

                } else {
                    return false;

                }
            }
     /*String emailPattern="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    Pattern pattern= Pattern.compile(emailPattern);
        Matcher matcher=pattern.matcher(email);
        return  matcher.matches();*/

            protected boolean validatePassword(String password)

            {
                if (password != null && password.length() > 6) {
                    return true;
                } else {
                    return false;
                }
            }


            // public void register
        });
    }
}

